import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:my_first_flutter/page/first_page.dart';
import 'package:my_first_flutter/page/hello_world_page.dart';
import 'package:my_first_flutter/page/second_page.dart';
import 'package:my_first_flutter/page_route_builder/custom_page_route.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.blue),
        useMaterial3: true,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState(this);
}

class _MyHomePageState extends State<MyHomePage> {
  static const String KEY_BUTTON_FIRST = "key_button_first";
  static const String KEY_BUTTON_SECOND = "key_button_second";
  static const String KEY_BUTTON_HELLO_WORLD = "key_button_hello_world";

  int _counter = 0;

  final StatefulWidget page;

  _MyHomePageState(this.page);

  void _onClicked(ValueKey key) {
    switch (key.value.toString()) {
      case KEY_BUTTON_FIRST:
        Navigator.push(
          context,
          CustomPageRoute(page, const FirstPage()),
        );
        log("_onClicked KEY_BUTTON_FIRST: ${key.value.toString()}");
        break;
      case KEY_BUTTON_SECOND:
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => const SecondPage()),
        );
        log("_onClicked KEY_BUTTON_SECOND: ${key.value.toString()}");
        break;
      case KEY_BUTTON_HELLO_WORLD:
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => const HelloWorldPage()),
        );
        log("_onClicked KEY_BUTTON_HELLO_WORLD: ${key.value.toString()}");
        break;
    }
  }

  ElevatedButton _createButton(String keyValue, Icon icon, String text) {
    ValueKey key = ValueKey<String>(keyValue);
    return ElevatedButton.icon(
      style: ElevatedButton.styleFrom(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(4.0),
          ),
          minimumSize: const Size(double.infinity, 48)),
      onPressed: () {
        _onClicked(key);
      },
      icon: icon,
      label: Text(text),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.primary,
        title: Text(
          widget.title,
          style: const TextStyle(color: Colors.white),
        ),
        centerTitle: true,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 4),
              child: _createButton(
                  KEY_BUTTON_FIRST, const Icon(Icons.face), "open first page"),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 4),
              child: _createButton(KEY_BUTTON_SECOND, const Icon(Icons.face_2),
                  "open second page"),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 4),
              child: _createButton(KEY_BUTTON_HELLO_WORLD, const Icon(Icons.face_3),
                  "open hello world page"),
            ),
          ],
        ),
      ),
    );

  }
}
