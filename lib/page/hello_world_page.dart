import 'package:flutter/material.dart';

class HelloWorldPage extends StatefulWidget {

  const HelloWorldPage({super.key,});

  @override
  State<HelloWorldPage> createState() => _HelloWorldState();

}

class _HelloWorldState extends State<HelloWorldPage> {

  int _counter = 0;

  void _incrementCounter() {
    print("_incrementCounter _counter: $_counter");
    setState(() {
      _counter++;
    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Theme
              .of(context)
              .colorScheme
              .primary,
          title: const Text(
            "HelloWorldPage",
            style: TextStyle(color: Colors.white),
          ),
          iconTheme: const IconThemeData(color: Colors.white),
          centerTitle: true,
        ),
        body: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 16, vertical: 4),
                  child: Text(
                    'You have pushed the button this many times:',
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 16, vertical: 4),
                  child: Text(
                      '$_counter',
                      style: Theme
                          .of(context)
                          .textTheme
                          .headlineMedium,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 15, vertical: 0),
                  child: TextButton(
                    // onPressed: () {
                    //   setState(() {
                    //     _counter++;
                    //   });
                    // },
                    onPressed: _incrementCounter,
                    child: const Text(
                      'test content by owen test content by owen test content by owen test content by owen 1 1 1 1 1 1 11',
                      style: TextStyle(
                        fontSize: 15,
                        decoration: TextDecoration.underline,
                      ),
                    ),
                  ),
                )
              ],
            )
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: _incrementCounter,
          tooltip: 'Increment',
          child: const Icon(Icons.add),
        ),
    );
  }
}

